class Subject < ApplicationRecord

  #Relationships
  has_many :pages

  #define some scopes (Queries defined in model)
  #scopes (queries are run when they are called)

  #Visible subjects
  scope :visible, lambda { where(:visible => true) }
  #Non visible subjects
  scope :invisible, lambda { where(:visible => false)}

  scope :sorted, lambda {order("position ASC")}
  scope :newest_first, lambda {order("created_at DESC")}

  #note query may be coming from user, so therefore we need to sanitize the user
  #input, in this case query
  scope :search, lambda {|query| where (["name LIKE ?", "%#{query}%"])}

  #this forces something to be entered for name
  validates_presence_of :name
  validates_length_of :name, :maximum => 255

end
