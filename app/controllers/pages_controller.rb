class PagesController < ApplicationController

  layout 'admin'

  before_action :confirm_logged_in

  before_action :find_subjects, :only => [:new, :create, :edit, :update]
  before_action :set_page_count, :only => [:new, :create, :edit, :update]

  def index
    #GET
    #using scopes defined in model, i.e. sorted.
    @pages = Page.sorted
  end

  def show
    #GET Request
    @page = Page.find(params[:id])
  end

  def new
    #GET reequest
    @page = Page.new
    @page_count = Page.count + 1

  end

  def create
    #POST
    @page = Page.new(page_params())
    if @page.save
      flash[:notice] = "Page Created Successfully."
      redirect_to(pages_path)
    else
      #@subjects = Subject.sorted # now moved into before_action
      @page_count = Page.count + 1
      render('new')
    end
  end

  def edit
    #GET request
    @page = Page.find(params[:id])
    #@page_count = Page.count # done after action is run
    #@subjects = Subject.sorted now done in another function
  end

  def update
    #GET $ POST\
    @page = Page.find(params[:id])
    if @page.update_attributes(page_params)
      flash[:notice] = "Page updated successfully"
      #this below goes to the show action.
      redirect_to(page_path)
    else
      #renders the form again
      #@page_count = Page.count #now done in controller filter after action
      #@subjects = Subject.sorted #now done in filters (before)
      render('edit')
    end
  end

  def delete
    #GET
    @page = Page.find(params[:id])
  end

  def destroy
    #GET
    @page = Page.find(params[:id])
    #DELETE
    @page.destroy
    flash[:notice] = "Page '#{@page.name}' destroyed successfully"
    redirect_to(pages_path)
  end

  private
  def page_params
    params.require(:page).permit(:subject_id, :name, :permalink, :position, :visible)
  end

  def find_subjects
    @subjects = Subject.sorted
  end

  def set_page_count
    @page_count = Page.count
    if params[:action] == 'new' || params[:action] == 'create'
      @page_count = Page.count + 1
    end
  end

end
