 class DemoController < ApplicationController

   layout 'application'

  def index
    #this is the default, written out for you just to see.
    render('index')
  end

   #don't need to have this defined, it's good practice.
   def hello
     @array = [1,2,3,4,5]
     @id = params['id']
     @page = params[:page] #whichever method u prefer.
     render('hello')
   end

   def other_hello
     redirect_to(:controller => 'demo', :action => 'index')
   end

   def google
     redirect_to('https://www.google.com')
   end

   def escape_output

   end

end
