class SubjectsController < ApplicationController

  #define layout in views -> layouts
  layout 'admin'

  before_action :confirm_logged_in

  def index
    #this is using the named scope
    logger.debug("**** DEBUG --> subjects controller - index *****")
    @subjects = Subject.sorted
    #it then looks for the index action
  end

  def show
    @subject = Subject.find(params[:id])
  end

  def new
    #form is expecting a subject object to create
    @subject = Subject.new({:name => 'Default'})
    @subject_count = Subject.count + 1
  end

  def create
    #this renders the create template

    #Instantiate a new object using form parameters
    @subject = Subject.new(subject_params)
    #save the object
    if @subject.save
      #If save succeeds, redirect to the index action
      flash[:notice] = "subject created successfully."
      redirect_to(subjects_path)
    else
      @subject_count = Subject.count + 1 #incase update fails, and 'new stub re-rendered, not new controller'
      render('new') #this just re-renders the form template
    end
  end

  def edit
    @subject = Subject.find(params[:id])
    @subject_count = Subject.count
  end

  def update
    #Find a new object using parameters
    @subject = Subject.find(params[:id])
    #Update the object
    if @subject.update_attributes(subject_params())
      #If save succeeds, redirect to the index action
      flash[:notice] = "Subject updated successfully."
      redirect_to(subject_path(@subject))
    else
      render('edit') #this just re-renders the form template
    end
  end

  def delete
    @subject = Subject.find(params[:id])
  end

  def destroy
    @subject = Subject.find(params[:id])
    @subject.destroy
    #remember the object is still "avaliable to rails right after deletion"
    flash[:notice] = "Subject '#{@subject.name}' destroyed successfully."
    redirect_to(subjects_path)
  end

  private

  def subject_params
    params.require(:subject).permit(:name, :position, :visible, :created_at)
  end


end
