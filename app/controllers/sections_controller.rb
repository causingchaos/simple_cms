class SectionsController < ApplicationController

  layout 'admin'

  before_action :confirm_logged_in

  def index
    #GET
    #using scopes defined in model, i.e. sorted.
    @sections = Section.sorted
  end

  def show
    #GET Request
    @section = Section.find(params[:id])
  end

  def new
    #GET reequest
    @section = Section.new
    @section_count = Section.count + 1
    @pages = Page.sorted
  end

  def create
    #POST
    @section = Section.new(section_params())
    if @section.save
      flash[:notice] = "Section Created Successfully."
      redirect_to(sections_path)
    else
      @section_count = Section.count + 1
      @pages = Page.sorted
      render('new')
    end
  end

  def edit
    #GET request
    @section = Section.find(params[:id])
    @section_count = Section.count
    @pages = Page.sorted
  end

  def update
    #GET $ POST\
    @section = Section.find(params[:id])
    if @section.update_attributes(section_params())
      flash[:notice] = "Page updated successfully"
      #this below goes to the show action.
      redirect_to(section_path(@section))
    else
      #renders the form again
      @section_count = Section.count + 1
      @pages = Page.sorted
      render('edit')
    end
  end

  def delete
    #GET
    @section = Section.find(params[:id])
  end

  def destroy
    #GET
    @section = Section.find(params[:id])
    #DELETE
    @section.destroy
    flash[:notice] = "Page '#{@section.name}' destroyed successfully"
    redirect_to(section_path)
  end

  private
  def section_params
    params.require(:section).permit(:page_id, :name, :position, :visible, :content_type, :content)
  end

end