module ApplicationHelper

  def error_messages_for(object)
    render(:partial => 'application/error_messages', :locals => {:object => object})
  end

  def status_tag(boolean, options={})
    options[:true_text] ||= ''   # ||= is refering to '' by default, if we don't specity
    #text then it defaults to empty string
    options[:false_text] ||= ''

    #we can add styles to these :classes with CSS. i.e.
    # true box text, image. false box text, image.
    if boolean
      content_tag(:span, options[:true_text], :class => 'status true')
    else
      content_tag(:span, options[:false_text], :class => 'status false')
    end
  end

end
