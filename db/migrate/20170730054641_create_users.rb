class CreateUsers < ActiveRecord::Migration[5.0]

  def up
    create_table :users do |t|
      #will auto create an users_id row (primary key) auto generating id column
      t.column "first_name", :string, :limit => 25
      t.string "last_name", :limit => 50
      t.string "email", :default => 255, :null => false
      t.string "password", :limit => 40
      t.timestamps
      #t.datetime "created_at"  these are long hand for above.
      #t.datetime "updated_at" these are long hand for above.
    end
  end

  def down
    drop_table :users
  end

end
