class CreatePages < ActiveRecord::Migration[5.0]

  def change
    create_table :pages do |t|
      t.integer "subject_id"
      t.string "name"
      t.string "permalink"
      t.integer "position"
      t.boolean "visible", :default => false
      #adds to the extra table
      t.timestamps
    end
    #the subject_id is the foreign key
    add_index("pages", "subject_id")
    #will make the lookup of the correct page.
    add_index("pages", "permalink")
  end

  def down
    #indexs drop automatically b/c they are part of the table.
    drop_tables :pages
  end
end
