class CreateSections < ActiveRecord::Migration[5.0]

  def up
    create_table :sections do |t|
      t.integer "subject_id" #foreign key
      t.string "name"
      t.integer "permalink"
      t.integer "position"
      t.boolean "visible"
      #t.string "content_type"
      #t.string "content"
      t.timestamps
    end
    #create foreign key on subject_id
    add_index("sections","subject_id")
  end

  def down
    remove_index("sections","subject_id")
    drop_table :sections
  end

end
