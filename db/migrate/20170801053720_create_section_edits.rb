class CreateSectionEdits < ActiveRecord::Migration[5.0]

  def up
    create_table :section_edits do |t|
      t.integer "admin_user_id" #FK from admin_user
      t.integer "section_id"    #FK from section
      t.string "summary"

      t.timestamps
    end
    #need indexs bc this is a link table
    add_index("section_edits",['admin_user_id','section_id'])
  end

  def down
    drop_table :section_edits
  end
end
