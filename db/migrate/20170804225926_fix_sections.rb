class FixSections < ActiveRecord::Migration[5.0]
  def change
    remove_index("sections","subject_id")
    rename_column :sections, :subject_id, :page_id
    remove_column :sections, :permalink
    add_index("sections","page_id")
  end
end
