Rails.application.routes.draw do


  ### Root routing #####
  #root 'test_controller#index'
  #root 'demo#index'
  root :to => 'public#index'

  #get 'public/show'
  get 'show/:permalink', :to => 'public#show'

  get 'admin', :to => 'access#menu' #note if user is already logged in go to the menu, otherwise it auto goes to login screen
  get 'access/menu'
  get 'access/login'
  post 'access/attempt_login'
  get 'access/logout'

  resource :admin_users, :except => [:show] do
    member do
      get :delete
    end
  end

  resources :subjects do
    member do
      get :delete
    end
  end

  resources :pages do
    member do
      get :delete  #need to add delete if we want a delete page.
    end
  end

  resources :sections do
    member do
      get :delete
    end
  end

  #match defaults from ruby, don't need them
  #get 'subjects/index'
  #get 'subjects/show'
  #get 'subjects/new'
  #get 'subjects/edit'
  #get 'subjects/delete'

  ### routes for the test_controller######
  #get 'test_controller/index'
  get ':controller(/:action(/:id))'

  ### routes for the demo controller##### #simple match route
  get 'demo/index'
  get 'demo/hello'
  get 'demo/other_hello'
  get 'demo/google'
  get 'demo/escape_output'
  #also removing default route bc/ it make go away in future versions of rails
  #get ':controller(/:action(/:id))'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
